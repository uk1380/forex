const request = require('request-promise');
const cheerio = require('cheerio');
const puppeteer = require('puppeteer');
const {URL, URLSearchParams} = require('url');
module.exports.get = function(url, query){
    if(!url){
        return Promise.reject('No url');
    }
    let options = {
        uri: url,
        transform: function (body){
            return cheerio.load(body);
        }
    }
    if(query){
        options.qs = query;
    }
    return request(options);
};
module.exports.dynamic_get = function(url, query){
    if(!url){
        return Promise.reject('No url');
    }
    let urlObj = new URL(url);
    if(query){
        urlObj.search = new URLSearchParams(query);
    }
    let urlStr = urlObj.toString();
    return new Promise( (resolve, reject)=>{
        puppeteer.launch()
        .then( browser=>{
            return browser.newPage();
        })
        .then(page=>{
            return page.goto(urlStr, {waitUntil: 'networkidle0'}).then( ()=>{
                return page.content();
            });
        })
        .then(html=>{
            console.log(html);
            resolve(cheerio.load(html));
        }).catch(reject)
    });

}