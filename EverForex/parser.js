function FormatNumberLength(num, length) {
    var r = "" + num;
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
}

let idLabelPrefix = (i, lbl)=>{
    return `rptRates_ctl${FormatNumberLength(i,2)}_lbl${lbl}`;
}

let elementIds = {
    pair: (i)=>{return idLabelPrefix(i, 'Product')},
    rates: {
        BidCC: (i)=>{return idLabelPrefix(i, 'BidCCRate')},
        AskCC: (i)=>{return idLabelPrefix(i, 'AskCCRate')},
        BidCT: (i)=>{return idLabelPrefix(i, 'BidCTRate')},
        AskCT: (i)=>{return idLabelPrefix(i, 'AskCTRate')},
        BidTC: (i)=>{return idLabelPrefix(i, 'BidTCRate')},
        AskTC: (i)=>{return idLabelPrefix(i, 'AskTCRate')},
        BidTT: (i)=>{return idLabelPrefix(i, 'BidTTRate')},
        AskTT: (i)=>{return idLabelPrefix(i, 'AskTTRate')},
    },
    time: (i)=>{return idLabelPrefix(i, 'LastUpdatedOn')}
};


module.exports = new function(){
    this.selectMarketTable = function($){
        return $('table[class="EntryContainer"]')[0];
    }
    this.getPageCount = function($){
        return $('#rptRates_ctl00_lblCount2').children().length;
    }
    this.getPairCount = function($){
        let table = this.selectMarketTable($);
        let tbody = table.children[1];
        return tbody.children.filter(tr=>{return tr.type === 'tag' && tr.attribs.bgcolor}).length;
    }
    this.getMarketInfo = function($){
        
        let resultList = [];
        let pairCount = this.getPairCount($);
        for(let idx = 1; idx <= pairCount; idx++){
            let result = {};
            let pairLblId = elementIds.pair(idx);
            let pairElement = $(`[id^=${pairLblId}] a`);

            let pairName = pairElement[0].children[0].data;
            if(pairName){
                result.pair = pairName;
            }
            result.rates = {};
            for(let rateKey of Object.keys(elementIds.rates)){
                let rate = $(`[id^=${elementIds.rates[rateKey](idx)}]`)[0].children[0].data;
                if(!rate){
                    rate = $(`[id^=${elementIds.rates[rateKey](idx)}]>span`)[0].children[0].data;
                }
                result.rates[rateKey] = rate || null;
            }
            let updateTime = $(`[id^=${elementIds.time(idx)}]`)[0].children[0].data;
            result.updateTime = updateTime;
            resultList.push(result);
        }
        return resultList;
    }
};