const {get} = require('../RequestCenter');
const parser = require('./parser');
const config = {
    url: 'http://everforex.com/cn/Rate/RatePanelFull.aspx'
};
async function EverForexClient(){
    let result;
    let $;
    try{
        $ = await get(config.url);
    }catch(err){
        console.error(err);
    }

    result = parser.getMarketInfo($);

    let page_count = parser.getPageCount($);
    if(page_count > 0){
        let promise_list = [];
        for(let i = 1; i <= page_count; i++){
            promise_list.push(get(config.url, {p:i}));
        }
        return new Promise( (resolve, reject)=>{
            Promise.all(promise_list).then( bodys =>{
                for(let body of bodys){
                    result = result.concat( parser.getMarketInfo(body));
                }
                resolve(result);
            }).catch(reject);
        })
    }else{
        return result;
    }
};

module.exports = EverForexClient;
