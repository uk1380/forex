const {dynamic_get} = require('../RequestCenter');
const parser = require('./parser');
const config = {
    url: 'http://www.accac.com.au/money/rate.html'
};

async function ACCAC_Client(){
    let $;
    try{
        $ = await dynamic_get(config.url);
        console.log($);
    }catch(err){
        console.error(err);
    }
}

ACCAC_Client();
