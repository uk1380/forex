const IG = require('ig-markets');

function USDTWDConverter(){
    this.igTrader = new IG('ecaec9843e46b7f3315afe80de98b0fa54d16621', 'ck0379', 'Jz!0426499520', false);
    this.getMarketInfo = function(pair = 'USDTWD'){
        return new Promise( (resolve, reject)=>{
            this.igTrader.findMarkets(pair,function (err,data) {
                if(err){
                    reject(err);
                } else if(data.markets && Array.isArray(data.markets)){
                    let idx = data.markets.findIndex(m=>{
                        return m.epic === 'CS.D.USDTWD.CFD.IP';
                    });
                    if(idx >= 0){
                        resolve(data.markets[idx]);
                    }else{
                        reject("epic 'CS.D.USDTWD.CFD.IP' not found in result", data);
                    }
                }
            });
        });
    }
    this.typeOfNaN = function(x){
        if (Number.isNaN(x)) {
          return 'Number NaN';
        }
        if (isNaN(x)) {
          return 'NaN';
        }
    }
    this.convert = function({USD, TWD} = {}){
        if(USD && this.typeOfNaN(USD)){
            return Promise.reject("Input value USD is not a Number");
        }else if(TWD && this.typeOfNaN(TWD)){
            return Promise.reject("Input value TWD is not a Number");
        }
        return new Promise( (resolve, reject)=>{
            this.getMarketInfo('USDTWD').then( (data)=>{
                let bid = data.bid;
                let offer = data.offer;
                if(!bid || !offer){
                    reject('bid or offer noe found in market data.');
                }
                let avgPrice = (bid + offer) / 2;
                if(USD){
                    resolve(Number(USD) * avgPrice);
                }else if(TWD){
                    resolve(Number(TWD) / avgPrice);
                }else{
                    reject("None of USD or TWD spicified");
                }
            }, reject);
        });
        
    }
    
}

module.exports = new USDTWDConverter();